//
// Created by herman on 11/23/19.
//

#include "Money.h"
#include "MoneyExceptions.h"
#include <cmath>

Money::Money(int pound, int shilling, double pence) {
    double dp = abs(pence) - floor(abs(pence));
    if (pound > 10e+9 || pound < -10e+9 || shilling > 20 || shilling < -20 ||
    pence > 12. || pence < -12. || (dp != 0 && dp != 0.5))
        throw IncorrectMoneyValue();
	double sign = 1.0;
	if (pound != 0) {
		if (shilling < 0) throw IncorrectMoneyValue();
		sign = pound / abs(pound);
	}
	else if (shilling != 0) {
		if (pence < 0) throw IncorrectMoneyValue();
		sign = shilling / abs(shilling);
	}
	else if (pence != 0) sign = pence / abs(pence);
    mValue = sign*(abs(pence) + 12 * (abs(shilling) + 20 * abs(pound)));
}

Money::Money() : mValue(0) {}

Money::Money(const Money &m2) : mValue(m2.mValue) {}

std::ostream &operator<<(std::ostream &ostr, const Money &money) {
	if (money.getSign() < 0)
		ostr << "-";
	if (money.getPounds())
		ostr << money.getPounds() << " pd. ";
	if (money.getShillings())
		ostr << money.getShillings() << " sh. ";
	if (money.getPence())
		ostr << money.getPence() << " p.";
	return ostr;
}

std::istream& operator>>(std::istream& istr, Money& money) {
    int pd, sh, p;
    istr >> pd;
    if (istr.fail()) return istr;
    istr >> sh;
    if (istr.fail()) return istr;
    istr >> p;
    if (istr.fail()) return istr;
    money = Money(pd, sh, p);
    return istr;
}

Money Money::operator+(const Money &other) const {
    Money newMoney;
    newMoney.mValue = this->mValue + other.mValue;
    return newMoney;
}

Money Money::operator-(const Money &other) const {
    Money newMoney;
    newMoney.mValue = this->mValue - other.mValue;
    return newMoney;
}

int Money::getPounds() const {
    return floor(std::abs(mValue) / 12 / 20);
}

int Money::getShillings() const {
    return floor((std::abs(mValue) - getPounds()*12*20)/12);
}

double Money::getPence() const {
    return std::abs(mValue) - getPounds()*12*20 - getShillings()*12;
}

double Money::toPence() const {
    return mValue;
}

double Money::toPounds() const {
    return mValue / 12 / 20;
}

double Money::toShillings() const {
    return mValue / 12;
}

double Money::getSign() const {
    return (mValue == 0) ? 0 : mValue / std::abs(mValue);
}

Money &Money::operator++() {
    mValue += 0.5;
    return *this;
}

Money Money::operator++(int) {
    auto old(*this);
    ++(*this);
    return old;
}

Money &Money::operator--() {
    mValue -= 0.5;
    return *this;
}

Money Money::operator--(int) {
    auto old(*this);
    --(*this);
    return old;
}

Money Money::operator-() const {
    Money newMoney;
    newMoney.mValue = -1 * mValue;
    return newMoney;
}

Money Money::operator+() const {
    Money newMoney;
    newMoney.mValue = mValue;
    return newMoney;
}

Money &Money::operator+=(const Money &other) {
    this->mValue += other.mValue;
    return *this;
}

Money &Money::operator-=(const Money &other) {
    this->mValue -= other.mValue;
    return *this;
}

bool Money::operator==(const Money &other) const {
    return this->mValue == other.mValue;
}

bool Money::operator!=(const Money &other) const {
    return !(*this == other);
}

bool Money::operator<(const Money &other) const {
    return this->mValue < other.mValue;
}
bool Money::operator<=(const Money &other) const {
    return *this < other || *this == other;
}
bool Money::operator>(const Money &other) const {
    return !(*this <= other);
}
bool Money::operator>=(const Money &other) const {
    return !(*this < other);
}
