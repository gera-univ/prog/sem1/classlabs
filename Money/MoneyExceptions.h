//
// Created by herman on 11/23/19.
//

#ifndef MONEY_MONEYEXCEPTIONS_H
#define MONEY_MONEYEXCEPTIONS_H

#include <exception>

class IncorrectMoneyValue : public std::exception {};


#endif //MONEY_MONEYEXCEPTIONS_H
