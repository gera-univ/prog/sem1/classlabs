//
// Created by herman on 11/23/19.
//

#ifndef MONEY_MONEY_H
#define MONEY_MONEY_H

#include <iostream>

class Money {
    double mValue; // money value in pence
public:
    Money();
    Money(const Money&);
    Money(int pound, int shilling, double pence);

    [[nodiscard]] double getSign() const;
    [[nodiscard]] int getPounds() const;
    [[nodiscard]] int getShillings() const;
    [[nodiscard]] double getPence() const;
    [[nodiscard]] double toPounds() const;
    [[nodiscard]] double toPence() const;
    [[nodiscard]] double toShillings() const;

    Money operator-() const;
    Money operator+() const;
    Money& operator++();
    Money operator++(int);
    Money& operator--();
    Money operator--(int);

    Money operator+(const Money&) const;
    Money operator-(const Money&) const;
    Money& operator+=(const Money&);
    Money& operator-=(const Money&);

    bool operator==(const Money&) const;
    bool operator!=(const Money&) const;
    bool operator<(const Money&) const;
    bool operator<=(const Money&) const;
    bool operator>(const Money&) const;
    bool operator>=(const Money&) const;

    // stream insertion operator
    friend std::ostream& operator<<(std::ostream& ostr, const Money& money);
    // stream extraction operator
    friend std::istream& operator>>(std::istream& istr, Money& money);
};

#endif //MONEY_MONEY_H
