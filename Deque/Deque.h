#ifndef DEQUE_DEQUE_H
#define DEQUE_DEQUE_H

#include "DequeExceptions.h"

#include <istream>
#include <ostream>

template<class Item>
class Deque {
    struct node {
        Item item;
        node *next;
        node *prev;

        node(Item item);

    };

    typedef node *link;
    link head, tail;
    int dSize;
public:
    Deque();

    Deque(const Deque&);

    virtual ~Deque();

    int size() const;

    int empty() const;

    Item back() const;

    Item front() const;

    Item pop_back();

    Item pop_front();

    void push_back(Item it);

    void push_front(Item);

    void clear();

    void operator=(const Deque &d);

    friend std::ostream &operator<<(std::ostream &ostr, const Deque &d) {
        auto n = d.tail;
        while (n != d.head) {
            ostr << n->item << " ";
            n = n->next;
        }
        if (n)
            ostr << n->item;
        return ostr;
    }

    friend std::istream &operator>>(std::istream &istr, Deque &d) {
        d.clear();
        Item it;
        while (istr >> it) {
            d.push_back(it);
        }
        return istr;
    }

};

template<class Item>
Deque<Item>::node::node(Item item): item(item), next(nullptr) {}

template<class Item>
Deque<Item>::Deque() {
    dSize = 0;
    head = nullptr;
    tail = nullptr;
}

template<class Item>
int Deque<Item>::empty() const {
    return head == nullptr;
}

template<class Item>
Item Deque<Item>::front() const {
    return tail->item;
}

template<class Item>
Item Deque<Item>::back() const {
    return head->item;
}

template<class Item>
void Deque<Item>::push_front(Item it) {
    link t = tail; // old tail
    tail = new node(it);
    if (head == nullptr)
        head = tail;
    else {
        t->prev = tail;
        tail->next = t;
    }
    ++dSize;
}

template<class Item>
void Deque<Item>::push_back(Item it) {
    link h = head; // old head
    head = new node(it);
    if (tail == nullptr)
        tail = head;
    else {
        h->next = head;
        head->prev = h;
    }
    ++dSize;
}

template<class Item>
Item Deque<Item>::pop_back() {
    if (empty())
        throw DequeUnderflowCondition();
    Item it = head->item;
    link h = head->prev;
    if (head == tail) tail = nullptr;
    delete head;
    head = h;
    --dSize;
    return it;
}

template<class Item>
Item Deque<Item>::pop_front() {
    if (empty())
        throw DequeUnderflowCondition();
    Item it = tail->item;
    link t = tail->next;
    if (head == tail) head = nullptr;
    delete tail;
    tail = t;
    --dSize;
    return it;
}

template<class Item>
int Deque<Item>::size() const {
    return dSize;
}

template<class Item>
void Deque<Item>::clear() {
    while (head != nullptr) {
        link tmp = head;
        head = head->next;
        delete tmp;
    }
    dSize = 0;
    tail = nullptr;
}

template<class Item>
Deque<Item>::~Deque() {
    clear();
}

template<class Item>
Deque<Item>::Deque(const Deque &d) {
    auto n = d.tail;
    while (n != d.head) {
        push_back(n->item);
        n = n->next;
    }
    if (n) this->push_back(n->item);
}

template<class Item>
void Deque<Item>::operator=(const Deque &d) {
    clear();
    auto n = d.tail;
    while (n != d.head) {
        push_back(n->item);
        n = n->next;
    }
    if (n) this->push_back(n->item);
}

#endif //DEQUE_DEQUE_H
