using System;

namespace FractionCSharp
{
    public class Fraction : IComparable
    {
        private long _denominator;
        private long _numerator;

        public Fraction(long numerator = 0, long denominator = 1)
        {
            _numerator = numerator;
            _denominator = denominator;

            CheckDenominator(_denominator);

            Reduce();
        }

        public long Numerator
        {
            get => _numerator;
            set
            {
                _numerator = value;
                Reduce();
            }
        }

        public long Denominator
        {
            get => _denominator;
            set
            {
                CheckDenominator(value);

                _denominator = value;
                Reduce();
            }
        }

        public int CompareTo(object obj)
        {
            if (obj is Fraction other)
            {
                var commonDenominator = FractionUtils.LCD(_denominator, other._denominator);
                var k1 = commonDenominator / _denominator;
                var k2 = commonDenominator / other._denominator;
                return (_numerator * k1).CompareTo(other._numerator * k2);
            }

            return -1;
        }

        private static void CheckDenominator(long value)
        {
            if (value < 0) throw new InvalidOperationException();

            if (value == 0) throw new DivideByZeroException();
        }

        public static explicit operator double(Fraction f)
        {
            return (double) f._numerator / f._denominator;
        }

        public static Fraction operator +(Fraction a, Fraction b)
        {
            var commonDenominator = FractionUtils.LCD(a._denominator, b._denominator);
            var kA = commonDenominator / a._denominator;
            var kB = commonDenominator / b._denominator;
            return new Fraction(a._numerator * kA + b._numerator * kB, commonDenominator);
        }

        public static Fraction operator -(Fraction a, Fraction b)
        {
            b._numerator *= -1;
            return a + b;
        }

        public static Fraction operator *(Fraction a, Fraction b)
        {
            return new Fraction(a._numerator * b._numerator, a._denominator * b._denominator);
        }

        public static Fraction operator /(Fraction a, Fraction b)
        {
            return a * new Fraction(b._denominator, b._numerator);
        }

        public static Fraction operator +(Fraction f)
        {
            return f;
        }

        public static Fraction operator -(Fraction f)
        {
            return new Fraction(f._numerator * -1, f._denominator);
        }

        public override string ToString()
        {
            return $"{_numerator} / {_denominator}";
        }

        public override bool Equals(object f)
        {
            if (!(f is Fraction)) return false;
            return this == (Fraction) f;
        }

        public void Deconstruct(out long numerator, out long denominator)
        {
            numerator = _numerator;
            denominator = _denominator;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_denominator, _numerator);
        }

        public static bool operator <(Fraction a, Fraction b)
        {
            return Compare(a, b) < 0;
        }

        public static bool operator >(Fraction a, Fraction b)
        {
            return Compare(a, b) > 0;
        }

        public static bool operator ==(Fraction a, Fraction b)
        {
            return Compare(a, b) == 0;
        }

        public static bool operator !=(Fraction a, Fraction b)
        {
            return Compare(a, b) != 0;
        }

        public static bool operator <=(Fraction a, Fraction b)
        {
            return Compare(a, b) <= 0;
        }

        public static bool operator >=(Fraction a, Fraction b)
        {
            return Compare(a, b) >= 0;
        }

        public static int Compare(Fraction a, Fraction b)
        {
            if (ReferenceEquals(a, b)) return 0;
            if ((object) a == null) return -1;
            if ((object) b == null) return 1;
            return a.CompareTo(b);
        }

        private void Reduce()
        {
            var divisor = FractionUtils.GCD(Numerator, Denominator);
            _numerator /= divisor;
            _denominator /= divisor;
            if (Denominator < 0)
            {
                _numerator *= -1;
                _denominator *= -1;
            }
        }
    }
}