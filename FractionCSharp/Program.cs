﻿using System;

namespace FractionCSharp
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var a = new Fraction(2, 3);
            var b = new Fraction(-5, 6);

            Console.WriteLine($"+a\t{+a}");
            Console.WriteLine($"-a\t{-a}");
            Console.WriteLine($"+b\t{+b}");
            Console.WriteLine($"-b\t{-b}");
            Console.WriteLine($"a + b\t{a + b}");
            Console.WriteLine($"a - b\t{a - b}");
            Console.WriteLine($"a * b\t{a * b}");
            Console.WriteLine($"a / b\t{a / b}");
            Console.WriteLine($"a < b\t{a < b}");
            Console.WriteLine($"a <= b\t{a <= b}");
            Console.WriteLine($"a > b\t{a > b}");
            Console.WriteLine($"a >= b\t{a >= b}");
            Console.WriteLine($"a == b\t{a == b}");
            Console.WriteLine($"a.Equals(b)\t{a.Equals(b)}");
            Console.WriteLine($"a.CompareTo(b)\t{a.CompareTo(b)}");
            Console.WriteLine($"(double) a\t{(double) a}");
            Console.WriteLine($"(double) b\t{(double) b}");
            var (aN, aD) = a;
            Console.WriteLine($"Deconstructing a:\t{aN},{aD}");
            var (bN, bD) = b;
            Console.WriteLine($"Deconstructing b:\t{bN},{bD}");
        }
    }
}