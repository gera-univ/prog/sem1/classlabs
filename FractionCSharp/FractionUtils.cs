using System;

namespace FractionCSharp
{
    public static class FractionUtils
    {
        public static long GCD(long a, long b)
        {
            if (b == 0) throw new DivideByZeroException();
            while (b != 0)
            {
                long r = a % b;
                a = b;
                b = r;
            }

            return a;
        }

        public static long LCD(long a, long b)
        {
            return a * b / GCD(a, b);
        }
    }
}