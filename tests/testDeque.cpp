#include <iostream>
#include "../Deque/Deque.h"

using namespace std;

int main() {
    Deque<int> deq;
    deq.push_front(1);
    deq.push_front(0);
    deq.push_front(3);
    Deque<int> deq2(deq);
    cin >> deq;
    cout << deq << "\n" << deq2 << endl;
    deq2.pop_back();
    deq2.pop_front();
    deq2.push_front(1);
    deq2.push_front(2);
    deq2.push_back(3);
    cout << deq2 << endl;
    deq2 = deq;
    cout << deq2 << endl;
    deq2.clear();
}