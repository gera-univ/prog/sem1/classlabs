#include <iostream>
#include <iomanip>
#include "../Fraction/Fraction.h"
#include "../Fraction/FractionExceptions.h"

using namespace std;

int main() {
    Fraction f1, f2;
    cin.exceptions(ios::failbit);
	cout << "Enter fraction 1\n";
	try {
        cin >> f1;
    } catch (ios::failure) {
        cerr << "Fraction 1: Incorrect input!" << endl;
        return -1;
    } catch (FractionDivisionByZero) {
        cerr << "Fraction 1: Division by zero!" << endl;
        return -1;
	}
    cout << "Enter fraction 2\n";
    try {
        cin >> f2;
    } catch (ios::failure) {
        cerr << "Fraction 2: Incorrect input!" << endl;
        return -1;
    } catch (FractionDivisionByZero) {
        cerr << "Fraction 2: Division by zero!" << endl;
        return -1;
    }
    cout
    << "+f1\t" << +f1 << "\n+f2\t" << +f2 << "\n"
    << "-f1\t" << -f1 << "\n-f2\t" << -f2 << "\n"
    << "f1+f2\t" << f1+f2 << "\nf1-f2\t" << f1-f2 << "\n"
    << "f1*f2\t" << f1*f2 << "\nf1/f2\t" << f1/f2 << "\n"
    << boolalpha << "f1==f2\t" << (f1 == f2) << "\n"
    << "f1<f2\t" << (f1<f2) << "\nf1<=f2\t" << (f1<=f2) << "\n"
    << "f1>f2\t" << (f1>f2) << "\nf1>=f2\t" << (f1>=f2) << "\n";
    return 0;
}