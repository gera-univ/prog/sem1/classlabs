#include <iostream>
#include "../Money/Money.h"
#include "../Money/MoneyExceptions.h"

using namespace std;
int main() {
    Money cash1, cash2;
    cin.exceptions(ios::failbit);
    cout << "Enter money balance 1: (three integers PD SH P) \n";
    try {
        cin >> cash1;
    } catch (ios::failure) {
        cerr << "Cash 1: Incorrect input!" << endl;
        return -1;
    } catch (IncorrectMoneyValue) {
        cerr << "Cash 1: Incorrect monetary value!" << endl;
        return -1;
    }
    cout << "Enter money balance 2: (three integers PD SH P) \n";
    try {
        cin >> cash2;
    } catch (ios::failure) {
        cerr << "Cash 2: Incorrect input!" << endl;
        return -1;
    } catch (IncorrectMoneyValue) {
        cerr << "Cash 2: Incorrect monetary value!" << endl;
        return -1;
    }
    cout
            << "+cash1\t" << +cash1 << "\n+cash2\t" << +cash2 << "\n"
            << "-cash1\t" << -cash1 << "\n-cash2\t" << -cash2 << "\n"
            << "cash1+cash2\t" << cash1+cash2 << "\ncash1-cash2\t" << cash1-cash2 << "\n"
            << boolalpha << "cash1==cash2\t" << (cash1 == cash2) << "\n"
            << "cash1<cash2\t" << (cash1<cash2) << "\ncash1<=cash2\t" << (cash1<=cash2) << "\n"
            << "cash1>cash2\t" << (cash1>cash2) << "\ncash1>=cash2\t" << (cash1>=cash2) << "\n";
    return 0;
}