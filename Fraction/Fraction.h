//
// Created by herman on 11/24/19.
//

#ifndef CLASSLABS_FRACTION_H
#define CLASSLABS_FRACTION_H

#include <ostream>
#include <istream>

class Fraction {
    long numerator;
    long denominator;

    void reduce();

    static long gcd(long numerator, long denominator);
    static long lcd(long numerator, long denominator);

public:
    Fraction(long numerator, long denominator);
    Fraction(double value);
    Fraction();
    Fraction(const Fraction&);

    long getNumerator() const;

    void setNumerator(long numerator);

    long getDenominator() const;

    void setDenominator(long denominator);

    [[nodiscard]] Fraction reverse() const;

    [[nodiscard]] long double value() const;

    Fraction operator-() const;
    Fraction operator+() const;
    Fraction operator+(const Fraction&) const;
    Fraction operator-(const Fraction&) const;
    Fraction operator*(const Fraction&) const;
    Fraction operator/(const Fraction&) const;
    Fraction& operator+=(const Fraction&);
    Fraction& operator-=(const Fraction&);
    Fraction& operator*=(const Fraction&);
    Fraction& operator/=(const Fraction&);

    bool operator==(const Fraction&) const;
    bool operator!=(const Fraction&) const;
    bool operator<(const Fraction&) const;
    bool operator<=(const Fraction&) const;
    bool operator>(const Fraction&) const;
    bool operator>=(const Fraction&) const;

    // stream insertion operator
    friend std::ostream& operator<<(std::ostream& ostr, const Fraction& fraction);
    // stream extraction operator
    friend std::istream& operator>>(std::istream& istr, Fraction& fraction);
};


#endif //CLASSLABS_FRACTION_H
