//
// Created by herman on 11/24/19.
//

#include "Fraction.h"
#include "FractionExceptions.h"

#include <cmath>

Fraction::Fraction(long numerator, long denominator) : numerator(numerator), denominator(denominator) {
    if (denominator == 0)
        throw FractionDivisionByZero();
    reduce();
}

Fraction::Fraction(double value) {
    long bigNumber = 1;
    for (int i = 0; i < std::numeric_limits<long>::digits10; ++i)
        bigNumber *= 10;
    numerator = value*bigNumber;
    denominator = bigNumber;
    reduce();
}

Fraction::Fraction() : numerator(0), denominator(1) {}

Fraction::Fraction(const Fraction &f2) : numerator(f2.numerator), denominator(f2.denominator) {}

long double Fraction::value() const {
    return static_cast<long double>(numerator) / denominator;
}

Fraction Fraction::reverse() const {
    return Fraction(denominator, numerator);
}

long Fraction::getNumerator() const {
    return numerator;
}

void Fraction::setNumerator(long numerator) {
    Fraction::numerator = numerator;
}

long Fraction::getDenominator() const {
    return denominator;
}

void Fraction::setDenominator(long denominator) {
    Fraction::denominator = denominator;
}

void Fraction::reduce() {
    long divisor = gcd(numerator, denominator);
    numerator /= divisor;
    denominator /= divisor;
    if (denominator < 0) {
        numerator *= -1;
        denominator *= -1;
    }
}

long Fraction::gcd(long a, long b) {
    if (b == 0) throw FractionDivisionByZero();
    while (b != 0) {
        long r = a % b;
        a = b;
        b = r;
    }
    return a;
}

long Fraction::lcd(long a, long b) {
    return a * b / gcd(a, b);
}

Fraction& Fraction::operator*=(const Fraction& other) {
    this->numerator *= other.numerator;
    this->denominator *= other.denominator;
    reduce();
    return *this;
}

Fraction& Fraction::operator/=(const Fraction& other) {
    this->numerator *= other.denominator;
    this->denominator *= other.numerator;
    reduce();
    return *this;
}

Fraction Fraction::operator/(const Fraction& other) const {
    return Fraction(this->numerator*other.denominator, this->denominator*other.numerator);
}

Fraction Fraction::operator*(const Fraction& other) const {
    return Fraction(this->numerator*other.numerator, this->denominator*other.denominator);
}

Fraction Fraction::operator+(const Fraction& other) const {
    long commonDenominator = lcd(this->denominator, other.denominator);
    long k1 = commonDenominator/this->denominator;
    long k2 = commonDenominator/other.denominator;
    return Fraction(this->numerator * k1 + other.numerator * k2, commonDenominator);
}

Fraction& Fraction::operator+=(const Fraction& other) {
    long commonDenominator = lcd(this->denominator, other.denominator);
    long k1 = commonDenominator/this->denominator;
    long k2 = commonDenominator/other.denominator;
    this->numerator = this->numerator * k1 + other.numerator * k2;
    this->denominator = commonDenominator;
    return *this;
}

Fraction& Fraction::operator-=(const Fraction& other) {
    long commonDenominator = lcd(this->denominator, other.denominator);
    long k1 = commonDenominator/this->denominator;
    long k2 = commonDenominator/other.denominator;
    this->numerator = this->numerator * k1 - other.numerator * k2;
    this->denominator = commonDenominator;
    return *this;
}

Fraction Fraction::operator-(const Fraction& other) const {
    return *this + -other;
}

Fraction Fraction::operator-() const {
    return Fraction(-1*this->numerator, this->denominator);
}

Fraction Fraction::operator+() const {
    return Fraction(this->numerator, this->denominator);
}

std::ostream &operator<<(std::ostream &ostr, const Fraction &fraction) {
    if (fraction.numerator == 0 || abs(fraction.numerator) == fraction.denominator)
        ostr << fraction.numerator;
    else ostr << fraction.numerator << "/" << fraction.denominator;
    return ostr;
}

std::istream &operator>>(std::istream &istr, Fraction &fraction) {
    long numerator, denominator;
    istr >> numerator;
    if (istr.fail())
        return istr;
    char slash;
    istr >> slash;
    if (slash != '/') {
        istr.setstate(std::ios::failbit);
        return istr;
    }
    istr >> denominator;
    if (istr.fail())
        return istr;
    if (denominator == 0) throw FractionDivisionByZero();
    fraction.setNumerator(numerator);
    fraction.setDenominator(denominator);
    return istr;
}

bool Fraction::operator==(const Fraction &other) const {
    return (this->denominator == other.denominator) && (this->numerator == other.numerator);
}

bool Fraction::operator!=(const Fraction &other) const {
    return !(*this == other);
}

bool Fraction::operator<(const Fraction &other) const {
    long commonDenominator = lcd(this->denominator, other.denominator);
    long k1 = commonDenominator/this->denominator;
    long k2 = commonDenominator/other.denominator;
    return (this->numerator*k1) < (other.numerator*k2);
}

bool Fraction::operator>(const Fraction &other) const {
    return !(*this < other || *this == other);
}

bool Fraction::operator<=(const Fraction &other) const {
    return !(*this > other);
}

bool Fraction::operator>=(const Fraction &other) const {
    return !(*this < other);
}


