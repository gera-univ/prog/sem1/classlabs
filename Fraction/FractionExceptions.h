//
// Created by herman on 11/24/19.
//

#ifndef CLASSLABS_FRACTIONEXCEPTIONS_H
#define CLASSLABS_FRACTIONEXCEPTIONS_H

#include <exception>

class FractionDivisionByZero : public std::exception {};

#endif //CLASSLABS_FRACTIONEXCEPTIONS_H
